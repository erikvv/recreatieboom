import React from "react"
import { Layout } from '../components/layout'
import { css } from '@emotion/react'

const Home = () => (
    <Layout>
        <section>
            <h2>Kom recreëren in het Groene Woud </h2>
            <img src="/img/pages/index/quattro-cycles-5.jpg" 
                css={css`
                    width: 100%;
                    @media (min-width: 620px) {
                        width: 17rem;
                        float: left;
                        margin-right: .5rem;
                    }
                `}></img>
            <p>Als u een gezellig dagje of weekendje  uit wilt in Noord Brabant, tezamen met vrienden, familie of collegea, dan bent u bij ons aan het juiste adres. Ook voor teambuilding.</p>
            <p>Wij hebben genoeg mogelijkheden om uw uitstapje uniek te maken. Wat dacht u van een vierpersoons fiets huren, waarop u mooie dorpjes als Oirschot en Best kunt ontdekken. Of maak een schitterende rit door het Groene Woud met de paardentram.</p>
            <p>Wij bieden bovendien prettige accomodaties aan, zoals blokhutjes en hotelkamers en ook voor groepsaccomodatie kunt u bij ons terecht.</p>
            <p>Vanwege de kleinschaligheid wordt u altijd persoonlijk benaderd en natuurlijk staan de koffie of thee voor u klaar.</p>
            <p>Voor meer informatie en meer mogelijkheden nodigen wij u uit om te surfen op onze site. Wie weet wat u allemaal ontdekt.</p>
            <p css={{textAlign: 'right'}}>-- Jan en Marian van den Boom</p>
            <br />
            <p>
                Vlakbij de A2, de A50 en vliegveld Eindhoven Airport. 
                In de buurt van <a href="https://www.aquabest.nl/">Aqua Best</a>,&nbsp;
                <a href="https://toonvertier.nl/">Toon Vertier</a>,&nbsp;
                <a href="https://www.beeksebergen.nl">De Beekse Bergen</a>&nbsp;
                en&nbsp;
                <a href="https://www.efteling.com">De Efteling</a>.
            </p>
            <div css={css`
                display: flex;
                flex-wrap: wrap;
                margin: -.5rem;
                img {
                    width: 15rem;
                    flex-grow: 1;
                    margin: .5rem;
                }
            `}>
                <img src="/img/pages/index/blokhutjes.jpg"></img>
                <img src="/img/pages/index/ezels-borstelen.jpg"></img>
                <img src="/img/pages/index/hert.jpg"></img>
                <img src="/img/pages/index/rinske-hoogzwanger.jpg"></img>
                <img src="/img/pages/index/ezels.jpg"></img>
            </div>
            <p>De Hagelaar is niet alleen recreatieboerderij, maar ook een zorgboerderij. Meer informatie over onze zorgboerderij vindt u op <a href="http://www.dehagelaar.nl">www.dehagelaar.nl</a></p>
        </section>
    </Layout>
)

export default Home;
