import React from "react"
import { LeftImage, RightImage } from '../../components/image'
import { Layout } from '../../components/layout'
import { Quote } from '../../components/quote'

const QuatroBikesPage = () => (
    <Layout>
        <section>
            <h2>Quatro bike arrangement</h2>
            <p>
                Gezellig fietsen samen met uw gezin, vrienden of familie 
                door het schitterende natuurgebied Het Groene Woud. 
                Met een luxe en comfortabele quatro-bike voor vier personen,
                een gevulde picknickmand, 's avonds een BBQ en een overnachting in een blokhut 
                is dit een unieke ervaring!
            </p>
            <RightImage src="/img/pages/quatro-bikes/quattro cycles_4.jpg" />
            <p>
                Na een gastvrij ontvangst op onze boerderij staan de moderne vierpersoonsfietsen, 
                met voor ieder verstelbare stoelen en versnellingen, al te wachten. 
                Jong of oud, iedereen kan moeiteloos fietsen op deze bikes!
            </p>
            <p>
                Het ontdekken van het gebied kan met de uitgebreide routekaart, 
                maar misschien is een GPS,- of avonturentocht naar een schat iets voor u? 
                Hongerig na een middag fietsen kunt u gezellig op ons erf barbecueën.
            </p>
            <p>
                De nacht brengt u door in een knusse blokhut voor een goede nachtrust. Het suizen van de wind door de bomen werkt heerlijk ontspannend. De volgende ochtend kunt u genieten van een smakelijk en uitgebreid Bourgondisch ontbijt.
            </p>
            <RightImage src="/img/pages/quatro-bikes/quattro cycles_3.jpg" />
            <p>Inbegrepen:</p>
            <ul>
                <li>Quatro-bike gedurende één middag (na 14:00u en voor 16:30u terug zijn)</li>
                <li>Gevulde picknickmand voor 4 personen</li>
                <li>Overnachting in een blokhut</li>
                <li>Ontbijt</li>
                <li>Schoon beddengoed</li>
                <li>Routekaart</li>
            </ul>
            <p>Niet inbegrepen:</p>
            <ul>
                <li>Barbecue bij te boeken (prijs op aanvraag)</li>
            </ul>
            <Quote author="Een gast">
                Je kunt de 4 persoonsfietsen huren en daarnaast ook de gps-apparaatjes. Die twee samen zijn echt gaaf, we hadden echt veel lol gehad. Er zitten extra meerdere routes en verhalen over de buurt/omgeving. Die lees je onderweg, op de plek waar je bent!
            </Quote>
        </section>
    </Layout>
)

export default QuatroBikesPage;
