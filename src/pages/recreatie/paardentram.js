import { css } from '@emotion/react';
import React from "react"
import { Layout } from '../../components/layout'
import { Quote } from '../../components/quote';

const PaardentramPage = () => (
    <Layout>
        <section>
            <h2>Per antieke tram door het Groene Woud</h2>
            <img src="/img/pages/paardentram/tram.jpg" 
                css={css`
                    width: 100%;
                    @media (min-width: 620px) {
                        width: 17rem;
                        float: left;
                        margin-right: .5rem;
                    }
                `}></img>
            <p>
                Een rit in onze nostalgische tram is een bijzondere manier om met je familie, 
                vrienden of collegae een tocht te maken door het schitterende Groene Woud. 
                Jan kent de omgeving op zijn duimpje en tijdens de rit vertelt hij hierover leuke verhalen. 
                Er zijn diverse mogelijkheden voor arrangementen, bijvoorbeeld in combinatie met een bezoek aan Oirschot.
            </p>
            <p>
                Heeft u wensen dan zijn wij altijd bereid om met u mee te denken naar de mogelijkheden. 
                U kunt ons bellen of contact opnemen via e-mail.
            </p>
            <Quote author="Een gaste">
                Onze fotoclub zocht een originele manier om zich te verplaatsen. 
                We zijn bij boer Jan terecht gekomen en hij heeft ons met zijn tram rondgereden. 
                Eerst naar de Heilige eik en later zijn we nog in de natuur wat foto's gaan maken. 
                Het was enorm leuk om eens op deze manier een fotodag mee te maken.
            </Quote>
        </section>
    </Layout>
)

export default PaardentramPage;
