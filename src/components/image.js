import { css } from '@emotion/react'
import React from "react"

export const LeftImage = ({ ...props }) => 
    <img 
        {...props} 
        css={css`
            width: 100%;
            margin: 0 .5rem;
            @media (min-width: 620px) {
                width: 17rem;
                float: left;
                margin-right: .5rem;
            }
        `} />

export const RightImage = ({ ...props }) => 
    <img 
        {...props} 
        css={css`
            width: 100%;
            margin: 0 .5rem;
            @media (min-width: 620px) {
                width: 17rem;
                float: right;
                margin-right: .5rem;
            }
        `} />

