
import { css } from '@emotion/react';
import React from "react"

export const Quote = ({ children, author, ...props}) =>
    <blockquote {...props} css={css`
        font-style: italic;
        background-color: white;
        padding: 1rem;
        border-radius: 1rem;
        max-width: 25rem;
    `}>
        {children}
        <div css={css`
            text-align: right;
            font-style: normal;
            font-size: .875rem;
        `}>-- {author}</div>
    </blockquote>