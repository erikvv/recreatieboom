import { Link } from 'gatsby'
import { Router } from "@reach/router"
import React from "react"
import { css, ClassNames, Global } from '@emotion/react'

const yellowBackGround = css`
    background-color: rgb(216, 211, 117);
`

const freshGreen = 'rgb(42, 134, 103)';

const freshGreenBackground = css`
    background-color: ${freshGreen};
`

const NavCss = css`
    background-color: rgb(20, 71, 54);
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-content: center;
    text-align: center;
`

const Nav = ({children, className}) => 
    <nav css={NavCss} className={className}>
        {children}
    </nav>

const MainNavLink = ({ children, ...props }) => 
    <ClassNames>
        {({ css }) => 
            <Link 
                css={css`
                    color: white;
                    font-weight: bold;
                    padding: 1rem;
                    margin: 0 .4rem;
                `}
                activeClassName={css`
                    ${freshGreenBackground}
                `} 
                partiallyActive={true} 
                {...props}
            >
                {children}
            </Link>
        }
    </ClassNames>

const SubNavLink = ({ children, ...props }) => 
    <ClassNames>
        {({ css }) => 
            <Link
                css={css`
                    color: white;
                    font-weight: bold;
                    padding: 1rem;
                    margin: 0 .4rem;
                    font-size: .875rem;
                `}
                activeClassName={css`
                    ${yellowBackGround}
                    color: black !important;
                `} 
                partiallyActive={true} 
                {...props}
            >
                {children}
            </Link>
        }
    </ClassNames>

const SubNav = ({ children }) => 
    <Nav css={css`
        ${NavCss}
        background-color: ${freshGreen};
    `}>
        {children}
    </Nav>

const RecreatieMenu = () =>
    <SubNav>
        <SubNavLink to="/recreatie/paardentram">Paardentram</SubNavLink>
        <SubNavLink to="/recreatie/quatro-bikes">Quatro bikes</SubNavLink>
        <SubNavLink to="/recreatie/wandelen-met-de-ezeltjes">Wandelen met de ezeltjes</SubNavLink>
        <SubNavLink to="/recreatie/fotozoektocht">Fotozoektocht</SubNavLink>
        <SubNavLink to="/recreatie/zomer-aanbieding">Zomer aanbieding</SubNavLink>
    </SubNav>

export const Layout = ({ children }) => 
    <>
        <Global
            styles={css`
                body {
                    ${yellowBackGround}
                }
            `} />
        <header>
            <img className="head-img" src="/img/grasland.jpg" />
            <h1>Recreatie Boom</h1>
            <p>Ontspannen, spelen en genieten in het Groene Woud</p>
        </header>
        <Nav>
            <MainNavLink to="/" partiallyActive={false}>Start</MainNavLink>
            <MainNavLink to="/recreatie">Recreatie</MainNavLink>
            <MainNavLink to="/overnachten">Overnachten</MainNavLink>
            <MainNavLink to="/streek">Streek</MainNavLink>
            <MainNavLink to="/fotos">Foto's</MainNavLink>
            <MainNavLink to="/tarieven">Tarieven</MainNavLink>
            <MainNavLink to="/contact">Contact</MainNavLink>
        </Nav>
        <Router>
            <RecreatieMenu path="/recreatie/*" />
        </Router>
        <main>
            {children}
        </main>
    </>